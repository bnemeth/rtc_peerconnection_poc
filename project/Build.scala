import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "rtc_peerconnection_poc"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    javaCore,
    javaJdbc,
    javaEbean
  )

  resolvers += "Local Maven Repository" at "file:///home/balazsmaria/.m2/repository"


  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here      
  )

}
